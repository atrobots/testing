/*
 *File: bulletTestC2a.cpp
 *Author: AT Robots team
 *NOTE: still working on these tests
 */

 #include "bullet.h"
 #include "bullet.cpp"
 #include <iostream>
 #include <vector>
 #include <math.h>
 using namespace std;

void printBullet(Bullet bullet) {
  float x = bullet.getX();
  float y = bullet.getY();
  float direction = bullet.getDirection();
  cout << "X: " << x << ", Y: " << y << ", Direction: " << direction << ";";
}

void passes(bool v) {
  if (v == true) {
    cout << "passes\n";
  } else {
    cout << "fails\n";
  }
}

float check_degree(float degree) {
	if (degree >= 0 && degree < 360) {
		return degree;
	}
	if (degree < 0) {
		return -1;
	}
	float newDegree = fmod(degree, 360);
	return newDegree;
}


 int main (int argc, char* args[]){
   cout << "------------------------------------\n";
   cout << "bulletTestC2a.cpp\n\n";

   cout << "\nBullet can be created:\n";
   std::vector<Bullet> testing;
   Bullet a(100,100,0); testing.push_back(a);
   Bullet b(100,100,30); testing.push_back(b);
   Bullet c(100,100,60); testing.push_back(c);
   Bullet d(100,100,90); testing.push_back(d);
   Bullet e(100,100,120); testing.push_back(e);
   Bullet f(100,100,150); testing.push_back(f);
   Bullet g(100,100,180); testing.push_back(g);
   Bullet h(100,100,210); testing.push_back(h);
   Bullet i(100,100,240); testing.push_back(i);
   Bullet j(100,100,270); testing.push_back(j);
   Bullet k(100,100,300); testing.push_back(k);
   Bullet l(100,100,360); testing.push_back(l);

   for (auto & bullet : testing) {
     printBullet(bullet);
     passes(bullet.is_valid());
   }

   cout << "\nBullet takes only positive angles:\n";
   Bullet m(100,100,-400); printBullet(m);
   passes(m.is_valid());

   cout << "\ncheck_degree(float) converts degrees correctly:\n";
   cout << "589->" << check_degree(589) << "\n";
   cout << "361->" << check_degree(361) << "\n";

   cout << "\nBullet converts to appropriate angles:\n";
   Bullet n(100,100,589); printBullet(n);
   passes(n.is_valid());
   Bullet o(100,100,361); printBullet(o);
   passes(o.is_valid());

   cout << "\nBullet.collide_wall() gives appropriate value:\n";
   //x 15,540
   //y 15,465
   std::vector<Bullet> testing;
   Bullet p(14, 100, 180);


   cout << "\nBullet.nextX() gives appropriate value:\n";

   cout << "\nBullet.nextY() gives appropriate value:\n";

   cout << "\nBullet.move() appropriately updates the bullet (x,y):\n";

   cout << "\nBullet.despawn() sets the 'valid' flag to false:\n";





 }
