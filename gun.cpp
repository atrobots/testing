#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <math.h>

#include "gun.h"
#include "bullet.h"

#define OFFSETANGLE 15

Gun::Gun() {
	currentAngle = 0;
}

Bullet* Gun::shoot(float x, float y, float robotAngle) {
	//returns a bullet at the tip of the gun, pointing at the current angle of the gun
	canShoot = false;
	cooldown = fireRate;
	return new Bullet(x-5*cos(robotAngle*M_PI/180)+25*cos(currentAngle*M_PI/180),
		y+5*sin(robotAngle*M_PI/180)+25*sin(currentAngle*M_PI/180), 
		currentAngle);
}

void Gun::draw(SDL_Renderer* renderer, float x, float y, float robotAngle) {
	thickLineRGBA(renderer,			
		x-5*cos(robotAngle*M_PI/180),
		y+5*sin(robotAngle*M_PI/180), 
		x-5*cos(robotAngle*M_PI/180)+25*cos(currentAngle*M_PI/180), 
		y+5*sin(robotAngle*M_PI/180)+25*sin(currentAngle*M_PI/180),
		4, 0x00, 0x00, 0x00, 0xff);
}

void Gun::tick(float robotAngle) {
	if(!isAiming)
		targetAngle = robotAngle;
	if(currentAngle != targetAngle) {
            if(abs(currentAngle - targetAngle) > OFFSETANGLE){
            	if(currentAngle < targetAngle)
               		currentAngle += OFFSETANGLE;
            	else
			currentAngle -= OFFSETANGLE;
		}
            else
                currentAngle = targetAngle;
        }
	if(cooldown > 0) {
		cooldown--;
		if(cooldown == 0)
			canShoot = true;
	}
}

void Gun::aim(float degree) {
	targetAngle = degree;
	isAiming = true;
}

void Gun::unAim() {
	isAiming = false;
}

Gun::~Gun() {
    
}
