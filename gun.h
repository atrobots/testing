#ifndef GUN_H
#define GUN_H


#include <SDL2/SDL.h>
#include <math.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "bullet.h"

class Gun{
public:
    Gun();
    ~Gun();
    //void rotate();
    //void rotateWith(float);
    void aim(float);
    void unAim();
    void draw(SDL_Renderer*, float, float, float);
    Bullet* shoot(float, float, float);
    void tick(float);
    //void check(); 
    
private:
    //float x;
    //float y;
    //float robotAngle;
    float turnSpeed = 1.2;
    int fireRate = 3;
    int cooldown;
    //Robot & robot;
    bool canShoot;
    float targetAngle;
    float currentAngle;
    //std::vector<Bullet> bullets;
    bool isAiming;
};

#endif /* GUN_H */