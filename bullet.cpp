#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <math.h>

#include "bullet.h"

Bullet::Bullet(){
	this -> x = 250;
	this -> y = 250;
	this -> angle = 0;
	this -> speed = 1;
}

Bullet::Bullet(float x, float y, float angle){
	this -> x = x;
	this -> y = y;
	this -> angle = angle;
	this -> speed = 1;
}

void Bullet::go(){
	this -> x += speed * cos(angle * M_PI / 180);
	this -> y += speed * sin(angle * M_PI / 180);
}

void Bullet::draw(SDL_Renderer* renderer){
	thickLineRGBA(renderer, x, y,
		x - 10 * cos(angle * M_PI / 180), 
		y - 10 * sin(angle * M_PI / 180), 
		3, 0x00, 0x00, 0x00, 0xff);
}

Bullet::~Bullet(){
	
}