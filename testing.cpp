#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <random>
#include <string>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>
#include <cstdlib>

#include "gun.h"
#include "bullet.h"
#include "Robot.h"

using namespace std;

int main(int argc, char* args[]){
    int test = 0;
	SDL_Window* window = NULL;
	SDL_Surface* surface = NULL;
	SDL_Renderer* renderer = NULL;
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> x(1, 300);
    std::uniform_int_distribution<int> y(1, 200);
    std::uniform_int_distribution<int> angle(1, 360);
    std::uniform_int_distribution<int> color(0x00, 0xFF);
    std::uniform_int_distribution<int> speed(-75, 100);

	SDL_Init(SDL_INIT_VIDEO);

	window = SDL_CreateWindow("Bullet Testing", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 500, 500, SDL_WINDOW_SHOWN);
	surface = SDL_GetWindowSurface(window);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	
	Gun* gun = new Gun();
	float gunAngle = 0;

	std::vector<Bullet*> bullets;
	//for(int i=0; i<360; i+=30)
	//	bullets.push_back(new Bullet(250,250,i));
	

	while(true){
		SDL_Event e;
		if(SDL_PollEvent(&e)){
			if(e.type == SDL_QUIT || e.key.keysym.sym == SDLK_q)
				break;
		
                if (e.key.keysym.sym == SDLK_1){

		SDL_RenderClear(renderer);
		boxRGBA(renderer, 500, 0, 0, 500, 0xa0, 0xa0, 0xa0, 0xff);

		gunAngle += 15;
		gun -> aim(gunAngle);
		gun -> tick(0);
		bullets.push_back(gun -> shoot(255, 250, 0));

		for(int i=0; i<bullets.size(); i++){
			bullets.at(i) -> draw(renderer);
			bullets.at(i) -> go();
		}

		gun -> draw(renderer, 255, 250, 0);

		SDL_RenderPresent(renderer);

		SDL_Delay(30);
                }
               
               if (e.key.keysym.sym == SDLK_2){
                        SDL_RenderClear(renderer);
		boxRGBA(renderer, 500, 0, 0, 500, 0xa0, 0xa0, 0xa0, 0xff);

                        
                        Robot r = new Robot(x(rng),y(rng),angle(rng),color(rng),color(rng),color(rng));
                        while (true){
                        r->renderRobot(renderer);
                        r->targetSpeed = speed(rng);
                        r->targetAngle = angle(rng);
                        SDL_RenderPresent(renderer);

		SDL_Delay(30);
	}
                    }
        }
                }
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
	return 0;
}