#ifndef BULLET_H
#define BULLET_H

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <math.h>

class Bullet{
public:
	Bullet();
	Bullet(float, float, float);
	~Bullet();
	void go();
	void draw(SDL_Renderer*);
private:
	float x;
	float y;
	float angle;
	float speed;
};

#endif /* BULLET_H */